import subprocess
from celery import shared_task
import os
import json

from celery import Celery
from celery.utils.log import get_task_logger

local_dir = os.path.dirname(__file__)
logger = get_task_logger(__file__)
conf_file_name = 'celery-perfqe-conf.json'


def load_config(path) -> dict:
    if os.path.exists(path):
        with open(path) as f:
            return json.load(f)
    else:
        return {}


# Configuration priority:
# 1. /etc/ -> this cfg overwrites already loaded cfg
# 2. default conf from install directory
local = os.path.join(local_dir, conf_file_name)
wide = os.path.join('/etc', conf_file_name)
conf = {}
conf.update(load_config(local))
conf.update(load_config(wide))

app = Celery('executor')
try:
    app.conf.update(**conf['celery'])

except KeyError as e:
    logger.error('The celery part configuration is missing in configuration')
    raise e

except Exception as e:
    logger.error('An unknown error occurred during celery configuration')
    logger.error(e)
    raise e


@shared_task
def general(*args):
    subprocess.run(args)
